package com.example.edtechhackathon.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.edtechhackathon.R;

public class SignUpActivity extends AppCompatActivity {

    CardView studentRole;
    CardView partnerRole;
    CardView universityRole;
    CardView adminRole;

    TextView studentRoleText;
    TextView partnerRoleText;
    TextView universityRoleText;
    TextView adminRoleText;

    ImageView studentRoleIco;
    ImageView partnerRoleIco;
    ImageView universityRoleIco;
    ImageView adminRoleIco;

    LinearLayout studentRoleLayout;
    LinearLayout partnerRoleLayout;
    LinearLayout universityRoleLayout;
    LinearLayout adminRoleLayout;

    EditText email;
    EditText password;
    EditText repeatPassword;
    EditText name;

    private String userRole;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        init();
    }

    private void init(){
        email=findViewById(R.id.email_input);
        password=findViewById(R.id.password_input);
        repeatPassword=findViewById(R.id.password_repeat_input);
        name=findViewById(R.id.userName);

        studentRole=findViewById(R.id.studentRole);
        partnerRole=findViewById(R.id.partnerRole);
        universityRole=findViewById(R.id.universityRole);
        adminRole=findViewById(R.id.adminRole);

        studentRoleText=findViewById(R.id.studentText);
        partnerRoleText=findViewById(R.id.partnerText);
        universityRoleText=findViewById(R.id.universityText);
        adminRoleText=findViewById(R.id.adminText);

        studentRoleIco=findViewById(R.id.studentIco);
        partnerRoleIco=findViewById(R.id.partnerIco);
        universityRoleIco=findViewById(R.id.universityIco);
        adminRoleIco=findViewById(R.id.adminIco);

        studentRoleLayout=findViewById(R.id.studentRoleBg);
        partnerRoleLayout=findViewById(R.id.partnerRoleBg);
        universityRoleLayout=findViewById(R.id.universityRoleBg);
        adminRoleLayout=findViewById(R.id.adminBg);
    }

    public void onStudentRoleCardClick(View view) {
        //refresh
        partnerRoleLayout.setBackground(getDrawable(R.drawable.role_card_white_bg));
        partnerRoleIco.setImageResource(R.drawable.ic_partner);
        partnerRoleText.setTextColor(Color.BLACK);

        studentRoleLayout.setBackground(getDrawable(R.drawable.role_card_gradient_bg));
        studentRoleIco.setImageResource(R.drawable.ic_student_focused);
        studentRoleText.setTextColor(Color.WHITE);

        universityRoleLayout.setBackground(getDrawable(R.drawable.role_card_white_bg));
        universityRoleIco.setImageResource(R.drawable.ic_campus);
        universityRoleText.setTextColor(Color.BLACK);

        adminRoleLayout.setBackground(getDrawable(R.drawable.role_card_white_bg));
        adminRoleIco.setImageResource(R.drawable.ic_admin);
        adminRoleText.setTextColor(Color.BLACK);

        userRole = "student";
    }

    public void onPartnerRoleCardClick(View view) {
        //refresh
        studentRoleLayout.setBackground(getDrawable(R.drawable.role_card_white_bg));
        studentRoleIco.setImageResource(R.drawable.ic_student);
        studentRoleText.setTextColor(Color.BLACK);

        partnerRoleLayout.setBackground(getDrawable(R.drawable.role_card_gradient_bg));
        partnerRoleIco.setImageResource(R.drawable.ic_partner_focused);
        partnerRoleText.setTextColor(Color.WHITE);

        universityRoleLayout.setBackground(getDrawable(R.drawable.role_card_white_bg));
        universityRoleIco.setImageResource(R.drawable.ic_campus);
        universityRoleText.setTextColor(Color.BLACK);

        adminRoleLayout.setBackground(getDrawable(R.drawable.role_card_white_bg));
        adminRoleIco.setImageResource(R.drawable.ic_admin);
        adminRoleText.setTextColor(Color.BLACK);

        userRole = "partner";
    }

    public void onSignUpButtonClick(View view) {
        if(!userRole.equals("")&&!(email.getText().toString().equals(""))){
            if(password.getText().toString().equals(repeatPassword.getText().toString())){
                Intent intent = new Intent(this,LoadingAuthActivity.class);
                // true (если пользователь есть в системе), false (если нет в системе)
                // так как это регитсрация то передается параметр false
                // служит для распознавания intent в LoadingAuthActivity
                intent.putExtra("userContainStatus",false);
                intent.putExtra("email",email.getText().toString());
                intent.putExtra("password",password.getText().toString());
                intent.putExtra("role",userRole);
                intent.putExtra("name",name.getText().toString());
                startActivity(intent);
            }
            else{
                Toast.makeText(this, "Пароли не совпадают!", Toast.LENGTH_SHORT).show();
            }
        }
        else{
            Toast.makeText(this, "Заполните все поля и выберите роль!", Toast.LENGTH_SHORT).show();
        }
    }

    public void onUniversityRoleCardClick(View view) {
        studentRoleLayout.setBackground(getDrawable(R.drawable.role_card_white_bg));
        studentRoleIco.setImageResource(R.drawable.ic_student);
        studentRoleText.setTextColor(Color.BLACK);

        partnerRoleLayout.setBackground(getDrawable(R.drawable.role_card_white_bg));
        partnerRoleIco.setImageResource(R.drawable.ic_partner);
        partnerRoleText.setTextColor(Color.BLACK);

        universityRoleLayout.setBackground(getDrawable(R.drawable.role_card_gradient_bg));
        universityRoleIco.setImageResource(R.drawable.ic_campus_focused);
        universityRoleText.setTextColor(Color.WHITE);

        adminRoleLayout.setBackground(getDrawable(R.drawable.role_card_white_bg));
        adminRoleIco.setImageResource(R.drawable.ic_admin);
        adminRoleText.setTextColor(Color.BLACK);

        userRole = "university";
    }

    public void onAdminRoleCardClick(View view) {
        studentRoleLayout.setBackground(getDrawable(R.drawable.role_card_white_bg));
        studentRoleIco.setImageResource(R.drawable.ic_student);
        studentRoleText.setTextColor(Color.BLACK);

        partnerRoleLayout.setBackground(getDrawable(R.drawable.role_card_white_bg));
        partnerRoleIco.setImageResource(R.drawable.ic_partner);
        partnerRoleText.setTextColor(Color.BLACK);

        universityRoleLayout.setBackground(getDrawable(R.drawable.role_card_white_bg));
        universityRoleIco.setImageResource(R.drawable.ic_campus);
        universityRoleText.setTextColor(Color.BLACK);

        adminRoleLayout.setBackground(getDrawable(R.drawable.role_card_gradient_bg));
        adminRoleIco.setImageResource(R.drawable.ic_admin_focused);
        adminRoleText.setTextColor(Color.WHITE);

        userRole = "admin";
    }
}