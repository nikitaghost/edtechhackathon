package com.example.edtechhackathon.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.edtechhackathon.R;

public class SignInActivity extends AppCompatActivity {

    EditText emailEditText;
    EditText passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        init();
    }

    private void init(){
        emailEditText=findViewById(R.id.email_input);
        passwordEditText=findViewById(R.id.password_input);
    }

    public void onSignInButtonClick(View view) {
        Intent intent = new Intent(this,LoadingAuthActivity.class);
        intent.putExtra("email",emailEditText.getText().toString());
        intent.putExtra("password",passwordEditText.getText().toString());
        // true (если пользователь есть в системе), false (если нет в системе)
        // так как это авторизация то передается параметр true
        // служит для распознавания intent в LoadingAuthActivity
        intent.putExtra("userContainStatus",true);

        if(!(emailEditText.getText().toString().equals("")&&passwordEditText.getText().toString().equals(""))){
            startActivity(intent);
        }
        else{
            Toast.makeText(this, "Заполните поля!", Toast.LENGTH_SHORT).show();
        }
    }

    public void onSignUpActivity(View view) {
        Intent intent = new Intent(this,SignUpActivity.class);
        startActivity(intent);
    }
}