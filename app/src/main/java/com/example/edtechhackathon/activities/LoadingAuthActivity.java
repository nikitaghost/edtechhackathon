package com.example.edtechhackathon.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.edtechhackathon.DBHelper;
import com.example.edtechhackathon.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.yandex.mapkit.MapKitFactory;

public class LoadingAuthActivity extends AppCompatActivity {

    private boolean userContainStatus;
    private String email;
    private String password;
    private String role;
    private String name;
    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final String MAPKIT_API_KEY = "74454f0a-6b30-462c-a0e9-69fa27fd19d5";
        MapKitFactory.setApiKey(MAPKIT_API_KEY);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading_auth);
        init();
        acceptIntent();

        if(userContainStatus){
            signInWithEmailPassword(email,password);
        }
        else{
            signUpWithEmailPassword(email,password);
        }
    }

    private void init(){
        dbHelper = new DBHelper();
    }

    private void acceptIntent(){
        Intent intent = getIntent();
        userContainStatus = intent.getBooleanExtra("userContainStatus",false);
        email = intent.getStringExtra("email");
        password = intent.getStringExtra("password");
        if(!userContainStatus){
            role = intent.getStringExtra("role");
            name = intent.getStringExtra("name");
        }
    }

    public void signInWithEmailPassword(@NonNull String email, @NonNull String password){
        dbHelper.getmAuth().signInWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Toast.makeText(LoadingAuthActivity.this, "Авторизация успешна!", Toast.LENGTH_SHORT).show();
                    goToMenu();
                }
                else{
                    Toast.makeText(LoadingAuthActivity.this, "Ошибка Авторизации!", Toast.LENGTH_SHORT).show();
                    goBackToSign();
                }
            }
        });
    }

    public void signUpWithEmailPassword(@NonNull String email, @NonNull String password){

        dbHelper.getmAuth().createUserWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Toast.makeText(LoadingAuthActivity.this, "Регистрация успешна!", Toast.LENGTH_SHORT).show();
                    if(role.equals("student")){
                        dbHelper.writeToDataBase(getApplicationContext(),"USER_DATA","STUDENTS",dbHelper.getmAuth().getUid(),"Email",email);
                        dbHelper.writeToDataBase(getApplicationContext(),"USER_DATA","STUDENTS",dbHelper.getmAuth().getUid(),"Role",role);
                        dbHelper.writeToDataBase(getApplicationContext(),"USER_DATA","STUDENTS",dbHelper.getmAuth().getUid(),"Name",name);
                        dbHelper.writeToDataBase(getApplicationContext(),"USER_DATA","STUDENTS",dbHelper.getmAuth().getUid(),"city","");
                        dbHelper.writeToDataBase(getApplicationContext(),"USER_DATA","STUDENTS",dbHelper.getmAuth().getUid(),"date","");
                        dbHelper.writeToDataBase(getApplicationContext(),"USER_DATA","STUDENTS",dbHelper.getmAuth().getUid(),"university","");
                        dbHelper.writeToDataBase(getApplicationContext(),"USER_DATA","STUDENTS",dbHelper.getmAuth().getUid(),"specialization","");
                        goToMenu();
                    }
                    else if(role.equals("partner")){
                        dbHelper.writeToDataBase(getApplicationContext(),"USER_DATA","PARTNERS",dbHelper.getmAuth().getUid(),"Email",email);
                        dbHelper.writeToDataBase(getApplicationContext(),"USER_DATA","PARTNERS",dbHelper.getmAuth().getUid(),"Role",role);
                        dbHelper.writeToDataBase(getApplicationContext(),"USER_DATA","PARTNERS",dbHelper.getmAuth().getUid(),"Name",name);
                        goToMenu();
                    }
                    else if(role.equals("university")){
                        dbHelper.writeToDataBase(getApplicationContext(),"USER_DATA","UNIVERSITIES",dbHelper.getmAuth().getUid(),"Email",email);
                        dbHelper.writeToDataBase(getApplicationContext(),"USER_DATA","UNIVERSITIES",dbHelper.getmAuth().getUid(),"Role",role);
                        dbHelper.writeToDataBase(getApplicationContext(),"USER_DATA","UNIVERSITIES",dbHelper.getmAuth().getUid(),"Name",name);
                        goToMenu();
                    }
                    else if(role.equals("admin")){
                        dbHelper.writeToDataBase(getApplicationContext(),"USER_DATA","ADMINS",dbHelper.getmAuth().getUid(),"Email",email);
                        dbHelper.writeToDataBase(getApplicationContext(),"USER_DATA","ADMINS",dbHelper.getmAuth().getUid(),"Role",role);
                        dbHelper.writeToDataBase(getApplicationContext(),"USER_DATA","ADMINS",dbHelper.getmAuth().getUid(),"Name",name);
                        goToMenu();
                    }
                }
                else {
                    Toast.makeText(LoadingAuthActivity.this, "Что-то пошло не так!", Toast.LENGTH_SHORT).show();
                    goBackToSign();
                }
            }
        });
    }

    private void goBackToSign(){
        if(userContainStatus){
            Intent intent = new Intent(this, SignInActivity.class);
            startActivity(intent);
        }
        else{
            Intent intent = new Intent(this, SignUpActivity.class);
            startActivity(intent);
        }
    }

    private void goToMenu(){
        Intent intent = new Intent(this, BottomNavActivity.class);
        intent.putExtra("role",role);
        startActivity(intent);
    }
}