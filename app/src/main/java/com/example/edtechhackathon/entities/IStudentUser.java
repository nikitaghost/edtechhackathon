package com.example.edtechhackathon.entities;

public interface IStudentUser {

    public void respondToInternship();

    public void cancelRespondToInternship();

    public void createCV();

    public String getCVId();

    public void createInternshipFeedback();

}
