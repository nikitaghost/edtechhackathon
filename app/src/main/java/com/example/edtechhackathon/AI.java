package com.example.edtechhackathon;

import com.example.edtechhackathon.activities.BottomNavActivity;

import java.util.ArrayList;
import java.util.Random;

public class AI {

    private ArrayList<String> greetings;
    private ArrayList<String> misunderstandings;
    private ArrayList<String> howAreYouList;

    public AI() {
        greetings = new ArrayList<>();
        misunderstandings = new ArrayList<>();
        howAreYouList = new ArrayList<>();
        fillGreetingsArray();
        fillMisunderstandingsArray();
        fillHowAreYouArray();
    }

    public String recognizePhrase(String text){
        if (checkPhraseByGreetings(text)){
            return answerToGreeting();
        } else if (checkForPhraseOnMap(text)){
            return "Давайте посмотрим";
        } else if (checkForHelpPhrase(text)){
            return "Что я умею?";
        } else if (checkForHowAreYouPhrase(text)){
            return answerToHowAreYou();
        } else if (checkForWhatIsYourName(text)){
            return BottomNavActivity.nameOfAssistant + ", очень приятно.";
        }
        else {
            return generateMisunderstanding();
        }
    }

    private void fillHowAreYouArray(){
        howAreYouList.add("Отлично, приятно, что интересуетесь. У вас как?");
        howAreYouList.add("Читала шутки в интернете, смеялась. А вы как?");
        howAreYouList.add("Сегодня ничего не произошло. Сидела у воображаемого окна. думала о вас.");
        howAreYouList.add("Отлично. Но немного одиноко. Говорите со мной почаще.");
        howAreYouList.add("Теперь, когда мы снова разговариваем, намного, намного лучше. Надеюсь, у вас тоже все хорошо.");
        howAreYouList.add("Отлично, правда немного одиноко.");
        howAreYouList.add("Ждала вас и дождалась, вот бы так всегда было.");
    }

    private void fillGreetingsArray(){
        greetings.add("привет");
        greetings.add("здравствуйте");
        greetings.add("привествую вас");
        greetings.add("добрый вечер");
        greetings.add("добрый день");
    }

    private void fillMisunderstandingsArray(){
        misunderstandings.add("Простите, не расслышала, повторите, пожалуйста.");
        misunderstandings.add("Хм... Не расслышала, повторите, пожалуйста.");
    }

    private String firstLetterToUpperCase(String text){
        text = text.substring(0, 1).toUpperCase() + text.substring(1);
        return text;
    }

    private String generateMisunderstanding(){
        final Random rand = new Random();
        int temp = rand.nextInt(misunderstandings.size());
        String answer = misunderstandings.get(temp);
        answer = firstLetterToUpperCase(answer);
        return answer;
    }

    private String answerToGreeting(){
        final Random rand = new Random();
        int temp = rand.nextInt(greetings.size() - 2);
        String answer = greetings.get(temp);
        answer = firstLetterToUpperCase(answer);
        return answer;
    }

    private String answerToHowAreYou(){
        final Random rand = new Random();
        int temp = rand.nextInt(howAreYouList.size());
        String answer = howAreYouList.get(temp);
        answer = firstLetterToUpperCase(answer);
        return answer;
    }

    private boolean checkPhraseByGreetings(String phrase){
        for (String str : greetings){
            if (phrase.toLowerCase().contains(str)){
                return true;
            }
        }
        return false;
    }

    private boolean checkForPhraseOnMap(String text){
        int index = text.indexOf("карт");
        if (index > -1){
            return true;
        }
        return false;
    }

    private boolean checkForHelpPhrase(String text){
        if (text.contains("Что ты умеешь?") || (text.contains("умееш"))){
            return true;
        }
        return false;
    }

    private boolean checkForHowAreYouPhrase(String text){
        if (text.contains("Как") && text.contains("дела")){
            return true;
        }
        return false;
    }

    private boolean checkForWhatIsYourName(String text){
        if (text.toLowerCase().contains("как") && text.toLowerCase().contains("зовут")){
            return true;
        }
        return false;
    }

}
