package com.example.edtechhackathon.eventRecyclerView;

public class RecyclerViewItemEvent {

    private String title;
    private String description;
    private String city;
    private String date;
    private String ownerID;

    public RecyclerViewItemEvent(String title, String description, String city, String date, String ownerID) {
        this.title = title;
        this.description = description;
        this.city = city;
        this.date = date;
        this.ownerID = ownerID;
    }

    public RecyclerViewItemEvent(){
        this.title = "";
        this.description = "";
        this.city = "";
        this.date = "";
        ownerID = "";
    }

    public String getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(String ownerID) {
        this.ownerID = ownerID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
