package com.example.edtechhackathon.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.edtechhackathon.DBHelper;
import com.example.edtechhackathon.R;
import com.example.edtechhackathon.dashboardRecyclerView.RecyclerViewAdapter;
import com.example.edtechhackathon.dashboardRecyclerView.RecyclerViewItem;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DashboardStudentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DashboardStudentFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ArrayList<RecyclerViewItem> getRecyclerViewItems() {
        return recyclerViewItems;
    }

    public void setRecyclerViewItems(ArrayList<RecyclerViewItem> recyclerViewItems) {
        this.recyclerViewItems = recyclerViewItems;
    }

    RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    ArrayList<RecyclerViewItem> recyclerViewItems = new ArrayList<RecyclerViewItem>();
    DBHelper dbHelper = new DBHelper();


    public DashboardStudentFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DashboardStudentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DashboardStudentFragment newInstance(String param1, String param2) {
        DashboardStudentFragment fragment = new DashboardStudentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = getView().findViewById(R.id.recycler);
        adapter = new RecyclerViewAdapter(recyclerViewItems, getContext());
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        /*dbHelper.getDatabaseReference().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                RecyclerViewItemRequest recyclerViewItem = snapshot.child("USER_DATA").child("INTERNSHIPS").child("00000001").getValue(RecyclerViewItemRequest.class);
                recyclerViewItems.add(recyclerViewItem);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });*/
        dbHelper.getDatabaseReference().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot data : snapshot.child("USER_DATA").child("INTERNSHIPS").getChildren()){
                    RecyclerViewItem recyclerViewItem = data.getValue(RecyclerViewItem.class);
                    recyclerViewItems.add(recyclerViewItem);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard_student, container, false);
    }

    public void onClickSearchInternship(View view) {
        dbHelper.getDatabaseReference().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot data : snapshot.child("USER_DATA").child("INTERNSHIPS").getChildren()){
                    RecyclerViewItem recyclerViewItem = data.getValue(RecyclerViewItem.class);
                    recyclerViewItems.add(recyclerViewItem);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}